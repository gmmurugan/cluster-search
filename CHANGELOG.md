Changelog
=========

1.2.0
-----

Main:

- issue #1: search nodes within the same environment

Tests:

- Use kitchen-docker\_cli instead of kitchen-docker
- Use continuous integration with gitlab-ci
- Up-to-date kitchen.yml (image, no preparation, always use latest deps, etc.)
- Use gitlab-ci config template (20170202)

Misc:

- Use cookbook\_name alias instead of cluster\_search
- Handover maintenance to chef-platform group, change urls and maintainer

1.1.1
-----

- Fix markdown in changelog

1.1.0
-----

Misc:

- Switch kitchen driver to docker\_cli
- Fix all rubocop offenses
- Add Apache 2 License file
- Move changelog and contributing section from README to external files
- Clean headers

1.0.0
-----

- Initial version, tested with full kitchen suites
